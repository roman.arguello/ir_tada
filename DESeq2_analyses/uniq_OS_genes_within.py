import pandas as pd
import argparse
import collections, numpy
import csv

###################  pares the command line
parser = argparse.ArgumentParser(description='get the number of occupied fragments within each occupied gene from the Dam vs PolII-Dam data')
parser.add_argument('-f', '--file', required=True, type=str, help="input file (ie TopHits.31a.annotated.bed)")
parser.add_argument('-p', '--minpeak', dest='MP', required=True, type=int,help="minimum number of GATC fragments to consider a gene occupied")
parser.add_argument("-o1", "--outputfile1", dest='output1', type=str, help="output file for the GATC fragments per gene")
parser.add_argument("-o2", "--outputfile2", dest='output2', type=str, help="output bed file for the GATC fragments per gene")
args = parser.parse_args()


if (args.file):
    print("--------------\n- input file: ", args.file)
    print("- output file1: ", args.output1)
    print("- putput file2: ", args.output2)

###################
#print (args.output1)
peakcut = int(args.MP)
# prep the output files 
f1 = open(args.output1,"a")
f2 = open(args.output2,"a")

myG = []
with open(args.file, newline = '') as myF:                                                                                          
    myF = pd.read_csv(myF, delimiter='\t')

# check the data frame info
#print(myF.info())

myGenes = (myF.iloc[:,3]) # the column w/ the FB ids
myGenes = myGenes.values
print("- total GATC frags: ", len(myGenes))
UniGen = set(myGenes)
print("- unique genes: ", len(UniGen))

myGenes2 = numpy.array(myGenes)
#loop over uniq genes
minP = 0 # count the number 
for i in UniGen: 
    totocur = (myGenes2 == i).sum()
    if totocur >= peakcut:
        minP = minP + 1
        f1.write(i)
        f1.write("\t")
        f1.write(str(totocur))
        f1.write("\n")
        myG.append(i)
print("- uniq. genes w/ >", peakcut, " GATC frags. =", minP)
print("--------------\n")


with open(args.file, newline = '') as myF2:  
    myF2_reader = csv.reader(myF2, delimiter='\t')
    for fline in myF2_reader:
        if fline[3] in myG:
            result = '\t'.join(fline)
            f2.write(result)
            f2.write("\n")
            