# IR-Tada

This repository accompanies the manuscript, "Targeted molecular profiling of rare cell populations identifies olfactory sensory neuron fate and wiring determinants" (in revision). It contains the workflow for the analyses of TaDa data for 7 IR-expressing olfactory neuron populations, the code used, and intermediate files.

The source data for this work is available on ArrayExpress under accession number E-MTAB-8935.


**TaDa_workflow.org/html**: this file contains the overall descritpion of the analyses carried out in the paper, and provides details of running the programs and scripts that are not in the Rmd files (below).


---

## **subdirectories**



**Other_files**: 

    1. FlyTF_v2_filtered.tsv
        - List of transcription factors that have been filtered based on the 
        full FlyTF (v2) set
    2. GPCRs.txt, neuropeptides.txt, ion_channels.txt, neuron_projection_guidance.txt
       - functional category genes lists used in Fig. 2 based on FlyBase queries (flybase_search_terms.txt contains the search terms)
    3. sensory_genes.txt
        - set of chemosenory genes that use in this paper (particularly in Fig.1 supplement figures)

---

**DamID_analyses**: 

    1. DamID_pipeline_analyses.Rmd/html containing analyses of the DamID output.
    2. zipped file containig the output from the DamID pipeline that
    is used in the DamID_pipeline_analyses.Rmd file.

---

**DESeq2_analyses**: 

    1. DESeq2_on_TaDa.Rmd/html containing analyses of differential occupancy at GATC fragments.
    2. zipped file containig GATC_coverage files that are read into the above R scripts.
    3. zipped directory w/ intermidate files that are outputted w/ these scripts. 
    4. uniq_OS_genes_within.py: script that filters/summarizes GATC fragments w/in genes that are defined to be occupied (see TaDa_workflow.org/html).
    5. uniq_OS_genes_pairwise.py: script that filters/summarizes GATC fragments for genes that were defined to be differentially occupied (see TaDa_workflow.org/html).
    6. uniq_OS_genes_LRT.py: script that output the uniqe set of candiate  GATC fragments for genes based on DESeq2's LRT (see TaDa_workflow.org/html).
    7. uniq_OS_genes.py: script that checks that the genes/GATC fragments that are canidates are also in the list of genes that are sig. occupied (see TaDa_workflow.org/html).

---

**Pdm3_En_quant**:

    1. data file for quantification in Fig.5B and Fig.2 FigSupp1
    2. Rmd file for plotting the data and making comparisons
   

